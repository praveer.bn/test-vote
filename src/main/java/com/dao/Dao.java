package com.dao;

import com.entity.Party;
import com.entity.User;

import java.util.List;

interface Dao {
    void registerUser(User user);
    void registerParty(Party party);
    List<Party> showParty();

}
