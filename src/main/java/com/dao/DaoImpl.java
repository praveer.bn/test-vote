package com.dao;

import com.connection.JPAprovider;
import com.entity.Party;
import com.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class DaoImpl implements Dao{
    public void registerUser(User user) {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void registerParty(Party party) {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(party);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Party> showParty() {
        EntityManager entityManager = JPAprovider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Party> criteriaQuery = criteriaBuilder.createQuery(Party.class);
        Root<Party> from = criteriaQuery.from(Party.class);
        CriteriaQuery<Party> select = criteriaQuery.select(from);
        TypedQuery<Party> typedQuery = entityManager.createQuery(select);
        List<Party> list = typedQuery.getResultList();
        return list;
    }
}
