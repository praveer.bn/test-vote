package com.entity;

import javax.persistence.*;
import java.util.List;
@Entity
public class Party {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pid;
    private  String partyName;
    private String partySymbol;
    private String partySlogan;
    private int vote;
    @OneToMany(mappedBy = "party")
    private List<User> userList;

    public Party() {
    }

    public Party(String partyName, String partySymbol, String partySlogan, int vote) {
        this.partyName = partyName;
        this.partySymbol = partySymbol;
        this.partySlogan = partySlogan;
        this.vote = vote;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartySymbol() {
        return partySymbol;
    }

    public void setPartySymbol(String partySymbol) {
        this.partySymbol = partySymbol;
    }

    public String getPartySlogan() {
        return partySlogan;
    }

    public void setPartySlogan(String partySlogan) {
        this.partySlogan = partySlogan;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public String toString() {
        return "Party{" +
                "pid=" + pid +
                ", partyName='" + partyName + '\'' +
                ", partySymbol='" + partySymbol + '\'' +
                ", partySlogan='" + partySlogan + '\'' +
                ", vote=" + vote +
                ", userList=" + userList +
                '}';
    }
}
