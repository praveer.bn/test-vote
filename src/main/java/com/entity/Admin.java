package com.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
    @Id
    private int pass;
    private String name;

    public Admin(int pass, String name) {
        this.pass = pass;
        this.name = name;
    }

    public Admin() {
    }

    public int getPass() {
        return pass;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "pass=" + pass +
                ", name=" + name +
                '}';
    }
}
