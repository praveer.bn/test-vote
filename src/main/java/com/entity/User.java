package com.entity;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "userPhNo"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;
    private String userName;
    private int age;
    private long voteId;
    private long userPassword;
    private long userPhNo;
    private String status;
    @ManyToOne
    private Party party;

    public User() {
    }

    public User(String userName, int age, long voteId, long userPassword, long userPhNo, String status) {
        this.userName = userName;
        this.age = age;
        this.voteId = voteId;
        this.userPassword = userPassword;
        this.userPhNo = userPhNo;
        this.status = status;

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getVoteId() {
        return voteId;
    }

    public void setVoteId(long voteId) {
        this.voteId = voteId;
    }

    public long getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(long userPassword) {
        this.userPassword = userPassword;
    }

    public long getUserPhNo() {
        return userPhNo;
    }

    public void setUserPhNo(long userPhNo) {
        this.userPhNo = userPhNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", userName='" + userName + '\'' +
                ", age=" + age +
                ", voteId=" + voteId +
                ", userPassword=" + userPassword +
                ", userPhNo=" + userPhNo +
                ", status='" + status + '\'' +
                ", party=" + party +
                '}';
    }
}

