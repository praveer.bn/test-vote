package com.connection;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAprovider {
    public  static EntityManagerFactory entityManagerFactory;
    public static EntityManagerFactory getEntityManagerFactory(){
        if(entityManagerFactory==null){
            entityManagerFactory= Persistence.createEntityManagerFactory("praveer");
        }
        return entityManagerFactory;
    }
}
