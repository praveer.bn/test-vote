package com.servlet;

import com.connection.JPAprovider;
import com.dao.DaoImpl;
import com.entity.Admin;
import com.entity.Party;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/result")
public class Result  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        int pw= Integer.parseInt(req.getParameter("pw"));
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
       Admin admin= entityManager.find(Admin.class,pw);
        if(admin.getPass()==pw) {
            DaoImpl dao = new DaoImpl();
            List<Party> parties = dao.showParty();

            printWriter.print("<table border=2px>");
            printWriter.print("<th>PARTY NAME</th><th>PARTY LOGO </th><th>PARTY SLOGAn </th><th>VOTE COUNT</th>");
            for (Party s : parties) {
                printWriter.println("<tr><td>" + s.getPartyName() + "</td><td>" + s.getPartySymbol() + "</td><td>" + s.getPartySlogan() + "</td><td>" + s.getVote() + " </td></tr>");
            }
            printWriter.print("<br><a href='index.jsp'>MENU</a>");
        }else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("wrong password");
            requestDispatcher.include(req, resp);
        }
    }
}
