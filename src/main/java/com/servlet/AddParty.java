package com.servlet;

import com.connection.JPAprovider;
import com.dao.DaoImpl;
import com.entity.Party;
import com.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/addParty")
public class AddParty extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter = resp.getWriter();
        String name = req.getParameter("pName").toLowerCase();
        String psy = req.getParameter("pSy").toLowerCase();
        String psl = req.getParameter("pSl");
        EntityManager entityManager = JPAprovider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Party> criteriaQuery = criteriaBuilder.createQuery(Party.class);
        Root<Party> from = criteriaQuery.from(Party.class);
        CriteriaQuery<Party> select = criteriaQuery.select(from);
        TypedQuery<Party> typedQuery = entityManager.createQuery(select);
        List<Party> list = typedQuery.getResultList();
        List<Party> list1 = list.stream().filter(s -> s.getPartyName().contains(name)).collect(Collectors.toList());
        List<Party> list2 = list.stream().filter(s -> s.getPartySlogan().contains(psy)).collect(Collectors.toList());
        if (list1.size() == 0 && list2.size() == 0) {
            Party party = new Party(name, psy, psl, 0);
            DaoImpl dao = new DaoImpl();
            dao.registerParty(party);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("Party Added");
            requestDispatcher.include(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("NAME OR SYMBOL IS ALREADY PRESENT");
            requestDispatcher.include(req, resp);
        }
    }
}
