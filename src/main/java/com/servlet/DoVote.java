package com.servlet;

import com.connection.JPAprovider;
import com.dao.DaoImpl;
import com.entity.Party;
import com.entity.User;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@WebServlet("/doVote")
public class DoVote extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        LocalTime stime=LocalTime.of(12,30);
        LocalTime etime=LocalTime.of(12,50);
        LocalTime now=LocalTime.now();
        if(now.isAfter(stime)&&now.isBefore(etime)) {

            int id = Integer.parseInt(req.getParameter("uid"));
            long voteid = Long.parseLong(req.getParameter("vid"));
            long pw = Long.parseLong(req.getParameter("pw"));
            EntityManager entityManager = JPAprovider.getEntityManagerFactory().createEntityManager();
            User user = entityManager.find(User.class, id);
            if (user.getVoteId() == voteid && user.getUserPassword() == pw) {
                DaoImpl dao = new DaoImpl();
                List<Party> parties = dao.showParty();
                printWriter.print(" <form action=\"vote\">");
                printWriter.print("<table border=2px>");
                printWriter.print("<th>PARTY NAME</th><th>PARTY LOGO </th><th>PARTY SLOGAn </th><th>VOTE</th>");
                for (Party s : parties) {
                    printWriter.println("<tr><td>" + s.getPartyName() + "</td><td>" + s.getPartySymbol() + "</td><td>" + s.getPartySlogan() + "</td><td><th><input type=\"radio\" name=\"option\" value=" + s.getPid() + ">vote</th><br></td><td> </td></tr>");
                }
                printWriter.print(" <input type=\"submit\">");
                printWriter.print(" </form>");

            }
        }else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("Session Out");
            requestDispatcher.include(req, resp);
        }
    }
}
