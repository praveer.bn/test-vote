package com.servlet;

import com.connection.JPAprovider;
import com.entity.Party;
import com.entity.User;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/vote")
public class Vote extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        int id= Integer.parseInt(req.getParameter("option"));
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Party party=entityManager.find(Party.class,id);
        party.setVote(party.getVote()+1);
        entityManager.persist(party);
        entityManager.getTransaction().commit();
        entityManager.close();
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
        printWriter.print("vote is done");
        requestDispatcher.include(req, resp);

    }
}
