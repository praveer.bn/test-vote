package com.servlet;

import com.connection.JPAprovider;
import com.dao.DaoImpl;
import com.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@WebServlet("/addUser")
public class AddUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("uName");
        int age= Integer.parseInt(req.getParameter("age"));
        final long ph= Long.parseLong(req.getParameter("ph"));
        long pw= Long.parseLong(req.getParameter("pw"));
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery=criteriaBuilder.createQuery(User.class);
        Root<User> from=criteriaQuery.from(User.class);
        CriteriaQuery<User> select=criteriaQuery.select(from);
        TypedQuery<User> typedQuery=entityManager.createQuery(select);
        List<User> list=typedQuery.getResultList();
        List<User> list1=list.stream().filter(s->s.getUserPhNo()==ph).collect(Collectors.toList());
        if(list1.size()==0) {
            if (age >= 18 && age < 100) {
                Random random = new Random();
                long voteId = random.nextInt(89999999) + 10000000;
                User user = new User(name, age, voteId, pw, ph, "NO");
                DaoImpl dao = new DaoImpl();
                dao.registerUser(user);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("User Added");
                requestDispatcher.include(req, resp);
            } else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("Under Aged");
                requestDispatcher.include(req, resp);
            }
        }else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("Phone NUmber is already present");
            requestDispatcher.include(req, resp);
        }




    }
}
